# selfhosted

[![Screenshot][screenshot]](https://flickcluster.duckdns.org)

[screenshot]: img/2022-08-19-171921_1908x1068_scrot.png

This repository provides Docker Compose file that runs multiple containers on Docker. These are:
+ [Nginx](https://www.nginx.com/)
+ [Heimdall](https://github.com/linuxserver/Heimdall)
+ [Nextcloud](https://nextcloud.com/)
+ [Pi-hole](https://pi-hole.net/)
+ [drawio](https://drawio-app.com/)
+ [my-mind](https://github.com/ondras/my-mind)

## Requirements

+ git
+ docker and docker compose
+ firewalld or ufw
+ fail2ban

## How to use

### Security

Even though you may run this only inside your local network, but I recommend to setup either firewalld or ufw with fail2ban. After install these packages and configure start and enable the services to make it available at boot.

```
systemctl enable --now ufw.services
systemctl enable --now firewalld.services
systemctl enable --now fail2ban.service
```

#### Firewalld

+ To get list of available services: `firewall-cmd --get-services`
+ To list all the zones: `firewall-cmd --list-all-zones`
+ To add a service to a zone: `firewall-cmd --zone=public --add-service=ssh --permanent`
+ To reload Firewalld, so that changes will be applied: `firewall-cmd --reload`

#### UFW

+ To enable the UFW: `ufw enable`
+ To rate limiting SSH: `ufw limit ssh`
+ To rate limiting port 80: `ufw limit 80`

#### Fail2ban

Acording to the Archwiki:

> Fail2ban scans log files (e.g. /var/log/httpd/error_log) and bans IPs that show the malicious signs like too 
> many password failures, seeking for exploits, etc. Generally Fail2ban is then used to update firewall rules 
> to reject the IP addresses for a specified amount of time, although any other arbitrary action (e.g. sending 
> an email) could also be configured. 

To make fail2ban work nicely with Nginx container, we need to make Nginx logs available to fail2ban service. Therefore I use volume to archive this:

```
volumes:
    - ./docker/nginx/logs/:/var/log/nginx/:ro
```

and modify the `/etc/fail2ban/jail.local` (if file does not exist, you can create a new one)

```
[DEFAULT]

bantime.increment = true
bantime.rndtime = 15
bantime.maxtime = 3w
bantime.overalljails = true
bantime = 1d
ignoreip = 127.0.0.1/8 ::1 192.168.50.0/24

[sshd]

enabled = true
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s

[nginx-http-auth]

enabled = true
port    = http,https
logpath = /home/silentflick/selfhosted/docker/nginx/logs/error.log

[nginx-limit-req]

enabled = true
port    = http,https
logpath = /home/silentflick/selfhosted/docker/nginx/logs/error.log

[nginx-botsearch]

enabled = true
port    = http,https
logpath = /home/silentflick/selfhosted/docker/nginx/logs/error.log
maxretry = 2
```

Notice: the nginx container log will only exist after you start the container. You can create an empty `docker/nginx/logs/error.log` before you restart fail2ban.
Finally restart fail2ban: `systemctl restart fail2ban.service`

### Run the containers

There are three ways: 

+ LAN only: by doing this you can not access all services from outside your network (WAN). But this is the easiest and safest way.
+ LAN with VPN: It is a little bit complicated, you should setup the containers first and then add VPN to the system. For the VPN I recommend: OpenVPN, Wireguard or Tailscale.
+ Expose your server to the Internet: this is a very complicated and dangerous solution. Do not attempt to do this unless you have the experience.

Also you may need to edit `docker/nginx/nginx.conf`, `docker-compose.yml` and other files to suit your needs.

#### LAN only

First we need to edit the `docker/nginx/nginx.conf`.

+ Change the DNS servers IP in the `upstream pihole_dns` block to your DNS server IP.

```
upstream pihole_dns {
    hash $remote_addr;
    server your_dns_ip:53 fail_timeout=60s;
    server other_dns_ip:53 backup; # Other DNS server as a backup for our Pi-hole
}
```

+ Delete or comment out all lines contain `ssl`
+ Delete all servers that listen on port 80 in block `http {}`
+ Replace `listen 443` with `listen 80`
+ Comment or delete these two lines in `docker-compose.yml`:

```
- ./docker/certbot/www:/var/www/certbot/:ro
- ./docker/certbot/conf:/etc/nginx/ssl/:ro
```

If you want to access the services via https (port 443) you still need to comment or delete the two lines above in `docker-compose.yml` and create a self-signed certificate with this command and save to `docker/nginx/ssl`:

```
openssl req -x509 -newkey rsa:4096 -days 365 -keyout key_filename.pem -out cert_filename.pem
```

and only replace the `ssl_certificate` and `ssl_certificate_key` in `docker/nginx/nginx.conf`

```
ssl_certificate ssl/cert_filename.pem;
ssl_certificate_key ssl/key_filename.pem;
```

Because we create the certificate ourself, the web browser won't trust this and show a warning, therefore you may want to import the certificate. In Linux you can follow [this](https://wiki.archlinux.org/title/User:Grawity/Adding_a_trusted_CA_certificate#Personal_%E2%80%93_NSS_(Chromium,_Firefox)).

#### LAN with VPN

After running the containers in local network, now we need to setup VPN in order to access from outside. I recommend either Wireguard or Tailscale.

+ [Wireguard](https://yewtu.be/watch?v=SMF301vQqJo)
+ [Tailscale](https://tailscale.com/download/) and also get a [tailscale certificate](https://tailscale.com/blog/tls-certs/) for our services.

#### Expose your server to the Internet

You can run these containers in VPS, but in my case because I have another computer so I want to utilize it. Because my public IP address may change, therefore I use [Duck DNS](https://www.duckdns.org) to get around this. After create an account and a domain we need to forward port 80 and 443 from the router to port 80 and 443 on the server. If you have a static IP address and a domain, you can ignore this step.

Now we need to obtain a certificate from [Let's Encrypt](https://letsencrypt.org/). To make sure there won't be a problem, we will simplify our `docker-compose.yml` and `docker/nginx/nginx.conf` first. We only need nginx and certbot for this so comment out other services in `docker-compose.yml` and edit the `docker/nginx/nginx.conf`

```
server {
    listen 80;
    server_name flickcluster.duckdns.org;
    keepalive_timeout 5;
    server_tokens off;
    root /var/www/certbot;
    index index.html;
    location / {
    }
    location ~ /.well-know/acme-challenge/ {
        allow all;
        try_files $uri =404;
    }
}

server {
    listen 443 ssl;
    server_name flickcluster.duckdns.org;
    ssl_certificate /etc/nginx/dummy_cert.pem;
    ssl_certificate_key /etc/nginx/dummy_key.pem;
    location / {
    }
}
```

If we want to obtain the certificate we need Nginx to listen on both port 80 and 443, that means we need a certificate. Luckily we can create a dummy certificate from the command above and save it to `docker/certbot/conf`. Finally we can try to obtain the certificate with this command

```
docker-compose run --rm certbot certonly --webroot --webroot-path /var/www/certbot/ -d your_domain.com -e your_email@email --agree-tos
```

If it is successful, we can uncomment other services, change the ssl certificate and key to your new one and run.

```
docker-compose down
docker-compose up -d
```
